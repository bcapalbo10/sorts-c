#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define MUITO_GRANDE 1250000
#define GRANDE 50000
#define MEDIO 25000
#define PEQUENO 5000
#define TAM(x) (x == 0 ? "Pequeno" : (x == 1 ? "Medio" : (x == 2 ? "Grande" : "Muito Grande""")))

void selecionaSort();
void bubbleSort(int *, int);
void selectSort(int *, int);
void insertSort(int *, int);
void mergeSort(int *, int);
void quickSort(int *, int);
int bubbleSortExecution(int, int *);
int selectSortExecution(int, int *);
int insertSortExecution(int, int *);
void mergeSortExecution(int, int, int *, int *, int *);
void quickSortExecution(int, int, int *, int *, int *);
int *getVetorAleatorio(int);
int *getVetorOrdenado(int);
int *getVetorInvertido(int);
void printVetor(int *, int);

int main(int argc, char** argv) 
{
	selecionaSort();
	
	system("pause");
	return 0;
}

void selecionaSort()
{
	int tamanho[] = {PEQUENO, MEDIO, GRANDE, MUITO_GRANDE};

	printf("\n\nExecucao do bubbleSort: \n\n");
	for (int i = 0; i < 3; i++) {
		char tam[20];
		strcpy(tam,TAM(i));
		printf("Vetor: aleatorio\nTamanho: %s\n", tam);
		bubbleSort(getVetorAleatorio(tamanho[i]), tamanho[i]);
		printf("Vetor: Ordenado\nTamanho: %s\n", tam);
		bubbleSort(getVetorOrdenado(tamanho[i]), tamanho[i]);
		printf("Vetor: Invertido\nTamanho: %s\n", tam);
		bubbleSort(getVetorInvertido(tamanho[i]), tamanho[i]);
	}
	
	printf("\n\nExecucao do selectSort: \n\n");
	for (int i = 0; i < 3; i++) {
		char tam[20];
		strcpy(tam,TAM(i));
		printf("Vetor: aleatorio\nTamanho: %s\n", tam);
		selectSort(getVetorAleatorio(tamanho[i]), tamanho[i]);
		printf("Vetor: Ordenado\nTamanho: %s\n", tam);
		selectSort(getVetorOrdenado(tamanho[i]), tamanho[i]);
		printf("Vetor: Invertido\nTamanho: %s\n", tam);
		selectSort(getVetorInvertido(tamanho[i]), tamanho[i]);
	}

	printf("\n\nExecucao do insertSort: \n\n");
	for (int i = 0; i < 3; i++) {
		char tam[20];
		strcpy(tam,TAM(i));
		printf("Vetor: aleatorio\nTamanho: %s\n", tam);
		insertSort(getVetorAleatorio(tamanho[i]), tamanho[i]);
		printf("Vetor: Ordenado\nTamanho: %s\n", tam);
		insertSort(getVetorOrdenado(tamanho[i]), tamanho[i]);
		printf("Vetor: Invertido\nTamanho: %s\n", tam);
		insertSort(getVetorInvertido(tamanho[i]), tamanho[i]);
	}

	printf("\n\nExecucao do mergeSort: \n\n");
	for (int i = 0; i < 4; i++) {
		char tam[20];
		strcpy(tam,TAM(i));
		printf("Vetor: aleatorio\nTamanho: %s\n", tam);
		mergeSort(getVetorAleatorio(tamanho[i]), tamanho[i]);
		printf("Vetor: Ordenado\nTamanho: %s\n", tam);
		mergeSort(getVetorOrdenado(tamanho[i]), tamanho[i]);
		printf("Vetor: Invertido\nTamanho: %s\n", tam);
		mergeSort(getVetorInvertido(tamanho[i]), tamanho[i]);
	}

	printf("\n\nExecucao do quickSort: \n\n");
	for (int i = 0; i < 4; i++) {
		char tam[20];
		strcpy(tam,TAM(i));
		printf("Vetor: aleatorio\nTamanho: %s\n", tam);
		quickSort(getVetorAleatorio(tamanho[i]), tamanho[i]);
		printf("Vetor: Ordenado\nTamanho: %s\n", tam);
		quickSort(getVetorOrdenado(tamanho[i]), tamanho[i]);
		printf("Vetor: Invertido\nTamanho: %s\n", tam);
		quickSort(getVetorInvertido(tamanho[i]), tamanho[i]);
	}
	
	system("exit");
}

int *getVetorAleatorio(int tamanho)
{
	int * v = (int *) calloc (tamanho, sizeof (int));
	
	for (int i = 0; i < tamanho; i++){
		v[i] = rand() % tamanho;
	}
			
	return v;
}

int *getVetorOrdenado(int tamanho)
{
	int * v = (int *) calloc (tamanho, sizeof (int));
	
	for (int i = 0; i < tamanho; i++){
		v[i] = i;
	}
			
	return v;
}

int *getVetorInvertido(int tamanho)
{
	int * v = (int *) calloc (tamanho, sizeof (int));
	
	for (int i = 0; i < tamanho; i++){
		v[i] = tamanho - i;
	}
			
	return v;
}

void printVetor(int * vetor, int tamanho)
{
	for (int i = 0; i < tamanho; i++)
	{
		printf("%d, ", vetor[i]);
	}
	printf("\n\n\n");
}

void bubbleSort(int *vetor, int tamanho)
{
	int cont;
	//printVetor(vetor,tamanho);

	int inicio = GetTickCount();
	cont = bubbleSortExecution(tamanho, vetor);
	int fim = GetTickCount();

	//printVetor(vetor,tamanho);

	printf("\nTempo de execucao: %d\n", (fim-inicio));
	printf("Iteracoes: %d\n", cont);
	printf("-----------------------------------------------------\n");
}	

void selectSort(int *vetor, int tamanho)
{
	int cont;
	//printVetor(vetor,tamanho);

	int inicio = GetTickCount();
	cont = selectSortExecution(tamanho, vetor);
	int fim = GetTickCount();

	//printVetor(vetor,tamanho);

	printf("\nTempo de execucao: %d\n", (fim-inicio));
	printf("Iteracoes: %d\n", cont);
	printf("-----------------------------------------------------\n");
}

void insertSort(int *vetor, int tamanho)
{
	int cont;
	//printVetor(vetor,tamanho);

	int inicio = GetTickCount();
	cont = insertSortExecution(tamanho, vetor);
	int fim = GetTickCount();

	//printVetor(vetor,tamanho);

	printf("\nTempo de execucao: %d\n", (fim-inicio));
	printf("Iteracoes: %d\n", cont);
	printf("-----------------------------------------------------\n");
}	

void mergeSort(int *vetor, int tamanho)
{
	int contIt = 0, contRec = 0;
	//printVetor(vetor,tamanho);

	int inicio = GetTickCount();
	mergeSortExecution(0, tamanho-1, vetor, &contIt, &contRec);
	int fim = GetTickCount();

	//printVetor(vetor,tamanho);

	printf("\nTempo de execucao: %d\n", (fim-inicio));
	printf("Iteracoes: %d\n", contIt);
	printf("Chamadas: %d\n", contRec);
	printf("-----------------------------------------------------\n");
}	

void quickSort(int *vetor, int tamanho)
{
	int contIt = 0, contRec = 0;
	//printVetor(vetor,tamanho);

	int inicio = GetTickCount();
	quickSortExecution(0, tamanho-1, vetor, &contIt, &contRec);
	int fim = GetTickCount();

	//printVetor(vetor,tamanho);

	printf("\nTempo de execucao: %d\n", (fim-inicio));
	printf("Iteracoes: %d\n", contIt);
	printf("Chamadas: %d\n", contRec);
	printf("-----------------------------------------------------\n");
}

int bubbleSortExecution(int tamanho, int vetor[])
{
	int aux, cont = 0;
    
    for(int i=tamanho-1; i >= 1; i--) {  
        for( int j=0; j < i ; j++) {
            if(vetor[j]>vetor[j+1]) {
                aux = vetor[j];
                vetor[j] = vetor[j+1];
                vetor[j+1] = aux;
            }
            cont++;
        }
        cont++;
    }
    return cont;
}

int selectSortExecution(int tamanho, int vetor[]) 
{
	int min, aux, cont = 0;
	
	for (int i = 0; i < (tamanho-1); i++) {		
		min = i;		
		for (int j = (i+1); j < tamanho; j++) {
			if(vetor[j] < vetor[min]) {
				min = j;
			}
			cont++;
    	}    	
		if (i != min) {
	      	aux = vetor[i];
	      	vetor[i] = vetor[min];
	     	vetor[min] = aux;
    	}
    	cont++;
	}
	
	return cont;
}

int insertSortExecution(int tamanho, int vetor[])
{
	int j, atual, cont = 0;

	for (int i = 1; i < tamanho; i++) {
		atual = vetor[i];
		j = i - 1;

		while ((j >= 0) && (atual < vetor[j])) {
			vetor[j + 1] = vetor[j];
            j = j - 1;
            cont++;
		}
    
		vetor[j + 1] = atual;
		cont++;
	}
	
	return cont;
}

void mergeSortExecution(int esquerda, int direita, int vetor[], int *contIt, int *contRec)
{
	(*contRec)++;
	if(esquerda == direita) {
		return;
	}
	int pivo = (esquerda + direita) / 2;
	
	mergeSortExecution(esquerda, pivo, vetor, contIt, contRec);
	mergeSortExecution(pivo+1, direita, vetor, contIt, contRec);
	
	int i = esquerda;
	int j = pivo+1;
	int k = 0;

	int *aux = (int *) malloc(sizeof(int) * (direita-esquerda+2));

	while (i <= pivo || j <= direita) {	
		if (i == pivo + 1) {
			aux[k] = vetor[j];
			j++;
			k++;
		}		
		else if (j == direita + 1) { 
			aux[k] = vetor[i];
			i++;
			k++;
		}		
		else if (vetor[i] < vetor[j]) {
			aux[k] = vetor[i];
			i++;
			k++;
		}		
		else {
			aux[k] = vetor[j];
			j++;
			k++;
		}
		(*contIt)++;	
	}	
	for (i = esquerda; i <= direita; i++) {
		vetor[i] = aux[i - esquerda];
		(*contIt)++;
	}
	
	free(aux);
}

void quickSortExecution(int esquerda, int direita, int vetor[], int *contIt, int *contRec) 
{ 
	(*contRec)++;
	if (esquerda >= direita) {
		return;
	}	
	int pivo = (esquerda + direita) / 2;
	
	int i = esquerda;
	int j = direita;
	
	while (i < j) {	
		while (vetor[i] < vetor[pivo]) {
			i++;
			(*contIt)++;
		}
        while (vetor[j] > vetor[pivo]) {
        	j--;
			(*contIt)++;	
		}        
        if (i <= j) {			
			int aux = vetor[i];
			vetor[i] = vetor[j];
			vetor[j] = aux;			
			i++;
			j--;
		}
		(*contIt)++;	
	}	
	quickSortExecution(esquerda, j, vetor, contIt, contRec);
	quickSortExecution(i, direita, vetor, contIt, contRec);
}
